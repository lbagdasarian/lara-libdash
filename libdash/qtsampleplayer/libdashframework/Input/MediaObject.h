/*
 * MediaObject.h
 *****************************************************************************
 * Copyright (C) 2012, bitmovin Softwareentwicklung OG, All Rights Reserved
 *
 * Email: libdash-dev@vicky.bitmovin.net
 *
 * This source code and its use and distribution, is subject to the terms
 * and conditions of the applicable license agreement.
 *****************************************************************************/

#ifndef LIBDASH_FRAMEWORK_INPUT_MEDIAOBJECT_H_
#define LIBDASH_FRAMEWORK_INPUT_MEDIAOBJECT_H_

#include <chrono>
#include <iostream>
#include "IMPD.h"
#include "IDownloadObserver.h"
#include "IDASHMetrics.h"
#include "../Portable/MultiThreading.h"
#include "../libdashframework/MPD/TimeResolver.h"

#include "../libdashframework/helpers/Timing.h"
//#include "../helpers/Time.h"

namespace libdash
{
    namespace framework
    {
        namespace input
        {
            class MediaObject : public dash::network::IDownloadObserver, public dash::metrics::IDASHMetrics
            {
                public:
                    MediaObject             (dash::mpd::ISegment *segment, dash::mpd::IRepresentation *rep);
                    virtual ~MediaObject    ();
			int start;// =0;

                    bool                        StartDownload       ();
                    void                        AbortDownload       ();
                    void                        WaitFinished        ();
                    int                         Read                (uint8_t *data, size_t len);
                    int                         Peek                (uint8_t *data, size_t len);
                    int                         Peek                (uint8_t *data, size_t len, size_t offset);

float		GetMegabitsForBandwidth();
float		GetBandwidthOfVideo(int timewasted);
float		GetTotalTime();
float		GetRevisedTime(int timewasted);
                    dash::mpd::IRepresentation* GetRepresentation   ();

                    virtual void    OnDownloadStateChanged  (dash::network::DownloadState state);
                    virtual void    OnDownloadRateChanged   (uint64_t bytesDownloaded);
                    /*
                     * IDASHMetrics
                     */
                    const std::vector<dash::metrics::ITCPConnection *>&     GetTCPConnectionList    () const;
                    const std::vector<dash::metrics::IHTTPTransaction *>&   GetHTTPTransactionList  () const;
		uint64_t lastbytenum;
                private:
                    dash::mpd::ISegment             *segment;
                    dash::mpd::IRepresentation      *rep;
                    dash::network::DownloadState    state;
		//libdash::framework::mpd::TimeResolver	*startTime;
		//libdash::framework::helpers::Timing	*timingStart;
		std::chrono::time_point<std::chrono::high_resolution_clock> t0;
		std::chrono::time_point<std::chrono::high_resolution_clock> t1;

		//std::chrono::time_point<std::chrono::high_resolution_clock> twastedstart;

		//std::chrono::time_point<std::chrono::high_resolution_clock> twastedend;
		//std::chrono::time_point<std::chrono::high_resolution_clock> totaltwasted;
		int temp = 0;

		//std::chrono::time_point<std::chrono::high_resolution_clock> twastedstart2;

		//std::chrono::time_point<std::chrono::high_resolution_clock> twastedend2;
	//	std::chrono::time_point<std::chrono::high_resolution_clock> totaltwasted;
		//int temp2 = 0;
		//std::chrono::duration_cast<std::chrono::microseconds> durwasted;
		float temptimevar = 878;
                    mutable CRITICAL_SECTION    stateLock;
                    mutable CONDITION_VARIABLE  stateChanged;
            };
        }
    }
}

#endif /* LIBDASH_FRAMEWORK_INPUT_MEDIAOBJECT_H_ */
