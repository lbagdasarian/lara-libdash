/*
 * MediaObject.cpp
 *****************************************************************************
 * Copyright (C) 2012, bitmovin Softwareentwicklung OG, All Rights Reserved
 *
 * Email: libdash-dev@vicky.bitmovin.net
 *
 * This source code and its use and distribution, is subject to the terms
 * and conditions of the applicable license agreement.
 *****************************************************************************/

#include "MediaObject.h"
using namespace libdash::framework::input;
using namespace libdash::framework::mpd;
using namespace dash::mpd;
using namespace dash::network;
using namespace dash::metrics;
using namespace libdash::framework::helpers;

//using namespace dash::helpers;

MediaObject::MediaObject    (ISegment *segment, IRepresentation *rep) :
             segment        (segment),
             rep            (rep)
{
    InitializeConditionVariable (&this->stateChanged);
    InitializeCriticalSection   (&this->stateLock);
//	this->startTime = new TimeResolver();
	//this->timingStart = new Timing();

	//start= this->startTime->GetCurrentTimeInSec();
}
MediaObject::~MediaObject   ()
{
    if(this->state == IN_PROGRESS)
    {
        this->segment->AbortDownload();
        this->OnDownloadStateChanged(ABORTED);
    }
    this->segment->DetachDownloadObserver(this);
    this->WaitFinished();

    DeleteConditionVariable (&this->stateChanged);
    DeleteCriticalSection   (&this->stateLock);
}

bool                MediaObject::StartDownload          ()
{
  	this->segment->AttachDownloadObserver(this);
	//start = this->startTime->GetCurrentTimeInSec();
	//auto tmp = std::chrono::high_resolution_clock::now();
	t0 = std::chrono::high_resolution_clock::now();
	//totaltwasted = 0;
  	return this->segment->StartDownload();
}
void                MediaObject::AbortDownload          ()
{
	//twastedstart = std::chrono::high_resolution_clock::now();
    this->segment->AbortDownload();
    this->OnDownloadStateChanged(ABORTED);
	//twastedend= std::chrono::high_resolution_clock::now();
	//std::cout<<"Download aborted!!";
	//auto dur = twastedend-twastedstart;
	//auto i_millis=std::chrono::duration_cast<std::chrono::microseconds>(dur);
	//std::cout<<i_millis.count();
	//temp +=i_millis.count();
	//totaltwasted+=twastedend-twastedstart;
}
void                MediaObject::WaitFinished           ()
{
	
	//std::cout<<"Bytes for seg: "<< (int)lastbytenum;
	//int end = this->startTime->GetCurrentTimeInSec();
	//std::cout<<"\nend time: "<<end;
	//std::cout<<"\nLength of seg: "<<end-start;
	//int lengthofseg=end-start;
	
    EnterCriticalSection(&this->stateLock);
    while(this->state != COMPLETED && this->state != ABORTED)
//twastedstart2 = std::chrono::high_resolution_clock::now();
        SleepConditionVariableCS(&this->stateChanged, &this->stateLock, INFINITE);
//twastedend2 = std::chrono::high_resolution_clock::now();
//auto dur2 = twastedend2-twastedstart2;
//auto i_millis2 = std::chrono::duration_cast<std::chrono::microseconds>(dur2);
//temp2 += i_millis2.count();

//end of loop means download has ended

    LeaveCriticalSection(&this->stateLock);
t1 = std::chrono::high_resolution_clock::now(); //recording end time
	auto dur = t1-t0;
	auto i_millis=std::chrono::duration_cast<std::chrono::microseconds>(dur);
	auto f_secs =std::chrono::duration_cast<std::chrono::duration<float>>(dur);
//**	this->rep->setQualityRanking(this->GetBandwidthOfVideo((uint32_t)2);
	//won't let me use any representation methods b/c of irep instead of .cpp class
	
//std::cout<<"-->Total time lost!!: "<<temp2<<"\n";
	//std::cout<<"==>Time: "<<i_millis.count()<<"microseconds\n";

//	std::cout<<"==>Time: "<<f_secs.count()<<" seconds\n";		//printing time w/chrono
	//temp = 0;
//temp2 = 0;
//std::cout<<"\n\nBandwidth: "<< (float)lastbytenum/(float)f_secs.count()<<"\n\n";
temptimevar = (float)i_millis.count();//microseconds
//std::cout<<"\nTotal time: "<<temptimevar;
	}

float		MediaObject::GetMegabitsForBandwidth()
{
	return (float)lastbytenum*8;
}
float		MediaObject::GetTotalTime() //not being called right after waitfinished
{
	//still displays starting value which means that segment never finished downloaded
/**	while (temptimevar ==878)
		sleep(1);*/
	return (float)temptimevar;
}
float		MediaObject::GetBandwidthOfVideo(int timewasted) //not being called right after waitfinished
{
	//std::cout<<"\ntimedifference in seconds: "<<(temptimevar-timewasted)/1000000;
	return (float)(lastbytenum/((temptimevar-timewasted)/1000000));
}

float	MediaObject::GetRevisedTime(int timewasted)
{
	return (float)(temptimevar-timewasted);
}
int                 MediaObject::Read                   (uint8_t *data, size_t len)
{
    return this->segment->Read(data, len);
}
int                 MediaObject::Peek                   (uint8_t *data, size_t len)
{
    return this->segment->Peek(data, len);
}
int                 MediaObject::Peek                   (uint8_t *data, size_t len, size_t offset)
{
    return this->segment->Peek(data, len, offset);
}
IRepresentation*    MediaObject::GetRepresentation      ()
{
    return this->rep;
}
void                MediaObject::OnDownloadStateChanged (DownloadState state)//when is it called
{
    EnterCriticalSection(&this->stateLock);

    this->state = state;

    WakeAllConditionVariable(&this->stateChanged);
    LeaveCriticalSection(&this->stateLock);
}
void                MediaObject::OnDownloadRateChanged  (uint64_t bytesDownloaded)
{lastbytenum = bytesDownloaded;
//std::cout<<"thesearethebytesdownloaded: "<<lastbytenum;
		
}
const std::vector<ITCPConnection *>&    MediaObject::GetTCPConnectionList   () const
{
    return this->segment->GetTCPConnectionList();
}
const std::vector<IHTTPTransaction *>&  MediaObject::GetHTTPTransactionList () const
{
    return this->segment->GetHTTPTransactionList();
}
