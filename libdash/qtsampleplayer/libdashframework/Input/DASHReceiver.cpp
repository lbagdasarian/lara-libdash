/*
 * DASHReceiver.cpp
 *****************************************************************************
 * Copyright (C) 2012, bitmovin Softwareentwicklung OG, All Rights Reserved
 *
 * Email: libdash-dev@vicky.bitmovin.net
 *
 * This source code and its use and distribution, is subject to the terms
 * and conditions of the applicable license agreement.
 *****************************************************************************/

#include "DASHReceiver.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

using namespace libdash::framework::input;
using namespace libdash::framework::buffer;
using namespace libdash::framework::mpd;
using namespace dash::mpd;

DASHReceiver::DASHReceiver          (IMPD *mpd, IDASHReceiverObserver *obs, MediaObjectBuffer *buffer, uint32_t bufferSize) :
              mpd                   (mpd),
              period                (NULL),
              adaptationSet         (NULL),
              representation        (NULL),
              adaptationSetStream   (NULL),
              representationStream  (NULL),
              segmentNumber         (0),
              observer              (obs),
              buffer                (buffer),
              bufferSize            (bufferSize),
              isBuffering           (false)
{
    this->period                = this->mpd->GetPeriods().at(0);
    this->adaptationSet         = this->period->GetAdaptationSets().at(0);
    this->representation        = this->adaptationSet->GetRepresentation().at(0);

    this->adaptationSetStream   = new AdaptationSetStream(mpd, period, adaptationSet);
    this->representationStream  = adaptationSetStream->GetRepresentationStream(this->representation);//after every change to rep
//this->setrep(this->period, this->adaptationset, this->representation)
    this->segmentOffset         = CalculateSegmentOffset();

    InitializeCriticalSection(&this->monitorMutex);
}
DASHReceiver::~DASHReceiver     ()
{
    delete this->adaptationSetStream;
    DeleteCriticalSection(&this->monitorMutex);
}

bool                        DASHReceiver::Start                     ()
{
    if(this->isBuffering)
        return false;
	 this->isBuffering       = true;
    this->bufferingThread   = CreateThreadPortable (DoBuffering, this);
	//std::cout<<"\n\n"<<this->mpd->GetPeriods().at(0);//added 12:30
std::cout<<"\n\nsize"<<this->adaptationSet->GetRepresentation().size();//added 12:40
//this->representationStream = adaptation
    if(this->bufferingThread == NULL)
    {
        this->isBuffering = false;
        return false;
    }
//std::cout<<this->observer->multimediaStream->GetPosition();//added
    return true;
}
void                        DASHReceiver::Stop                      ()
{
    if(!this->isBuffering)
        return;

    this->isBuffering = false;
    this->buffer->SetEOS(true);

    if(this->bufferingThread != NULL)
    {
        JoinThread(this->bufferingThread);
        DestroyThreadPortable(this->bufferingThread);
    }
}
MediaObject*                DASHReceiver::GetNextSegment            ()
{
    ISegment *seg = NULL;

    if(this->segmentNumber >= this->representationStream->GetSize())
        return NULL;

    seg = this->representationStream->GetMediaSegment(this->segmentNumber + this->segmentOffset);

    if (seg != NULL)
    {
        MediaObject *media = new MediaObject(seg, this->representation);
        this->segmentNumber++;
	
        return media;
    }
    return NULL;
}
MediaObject*                DASHReceiver::GetSegment                (uint32_t segNum)
{
    ISegment *seg = NULL;

    if(segNum >= this->representationStream->GetSize())
        return NULL;

    seg = this->representationStream->GetMediaSegment(segNum + segmentOffset);

    if (seg != NULL)
    {
        MediaObject *media = new MediaObject(seg, this->representation);
        return media;
    }

    return NULL;
}
MediaObject*                DASHReceiver::GetInitSegment            ()
{
    ISegment *seg = NULL;

    seg = this->representationStream->GetInitializationSegment();

    if (seg != NULL)
    {
        MediaObject *media = new MediaObject(seg, this->representation);
        return media;
    }

    return NULL;
}
MediaObject*                DASHReceiver::FindInitSegment           (dash::mpd::IRepresentation *representation)
{
    if (!this->InitSegmentExists(representation))
        return NULL;

    return this->initSegments[representation];
}
uint32_t                    DASHReceiver::GetPosition               ()
{
    return this->segmentNumber;
}
void                        DASHReceiver::SetPosition               (uint32_t segmentNumber)
{
    // some logic here

    this->segmentNumber = segmentNumber;
}
void                        DASHReceiver::SetPositionInMsecs        (uint32_t milliSecs)
{
    // some logic here

    this->positionInMsecs = milliSecs;
}
void                        DASHReceiver::SetRepresentation         (IPeriod *period, IAdaptationSet *adaptationSet, IRepresentation *representation)
{
    EnterCriticalSection(&this->monitorMutex);

    bool periodChanged = false;

    if (this->representation == representation)
    {
        LeaveCriticalSection(&this->monitorMutex);
        return;
    }

    this->representation = representation;

    if (this->adaptationSet != adaptationSet)
    {
        this->adaptationSet = adaptationSet;

        if (this->period != period)
        {
            this->period = period;
            periodChanged = true;
        }

        delete this->adaptationSetStream;
        this->adaptationSetStream = NULL;

        this->adaptationSetStream = new AdaptationSetStream(this->mpd, this->period, this->adaptationSet);
    }

    this->representationStream  = this->adaptationSetStream->GetRepresentationStream(this->representation);
    this->DownloadInitSegment(this->representation);

    if (periodChanged)
    {
        this->segmentNumber = 0;
        this->CalculateSegmentOffset();
    }

    LeaveCriticalSection(&this->monitorMutex);
}
dash::mpd::IRepresentation* DASHReceiver::GetRepresentation         ()
{
    return this->representation;
}
uint32_t                    DASHReceiver::CalculateSegmentOffset    ()
{
    if (mpd->GetType() == "static")
        return 0;

    uint32_t firstSegNum = this->representationStream->GetFirstSegmentNumber();
    uint32_t currSegNum  = this->representationStream->GetCurrentSegmentNumber();
    uint32_t startSegNum = currSegNum - 2*bufferSize;

    return (startSegNum > firstSegNum) ? startSegNum : firstSegNum;
}
void                        DASHReceiver::NotifySegmentDownloaded   ()
{
    this->observer->OnSegmentDownloaded();//get media object
}
void                        DASHReceiver::DownloadInitSegment    (IRepresentation* rep)
{
    if (this->InitSegmentExists(rep))
        return;

    MediaObject *initSeg = NULL;
    initSeg = this->GetInitSegment();

    if (initSeg)
    {
        initSeg->StartDownload();
        this->initSegments[rep] = initSeg;
    }
}
bool                        DASHReceiver::InitSegmentExists      (IRepresentation* rep)
{
    if (this->initSegments.find(rep) != this->initSegments.end())
        return true;

    return false;
}

/* Thread that does the buffering of segments */
void*                       DASHReceiver::DoBuffering               (void *receiver)
{
    DASHReceiver *dashReceiver = (DASHReceiver *) receiver;

    dashReceiver->DownloadInitSegment(dashReceiver->GetRepresentation());

    MediaObject *media = dashReceiver->GetNextSegment();
//	dashReceiver=adaptationSet->GetRepresentation().at(0);
	ofstream outputfile;
outputfile.open("programdata.txt");

    while(media != NULL && dashReceiver->isBuffering)
    {//count 2 segs and change to highest
        media->StartDownload();

        if (!dashReceiver->buffer->PushBack(media))
            return NULL;

        media->WaitFinished();
std::cout<<"\nTime wasted from DashReceiver: "<<dashReceiver->buffer->getTimeWasted();
	std::cout<<"\nTotal time from DashReceiver: "<< media->GetTotalTime();
	std::cout<<"\nBytes for bandwidth from DashReceiver: "<<media->GetMegabitsForBandwidth();
	std::cout<<"\nBandwidth from DashReceiver: "<<media->GetBandwidthOfVideo(dashReceiver->buffer->getTimeWasted());
        
//media->GetBandwidthOfVideo(	dashReceiver->buffer->getTimeWasted()))
	//std::cout<<"\n\nquality: "<<dashReceiver->GetRepresentation()->GetQualityRanking();
	//dashReceiver->representation->setQualityRanking(media->GetBandwidthOfVideo((uint32_t)(dashReceiver->buffer->getTimeWasted())));
//	media->rep->setQualityRanking(media->GetBandwidthOfVideo((uint32_t)(dashReceiver->buffer->getTimeWasted())));

int reps[] = {101492,201384, 351043, 501122, 700873, 900606, 1100480, 1299746, 1598978, 1897661, 2296317, 2795939, 3392769, 4493282};
int quality = media->GetBandwidthOfVideo(dashReceiver->buffer->getTimeWasted());
int diff = quality-reps[0];
int counter = 1;
int selIndx=0;

while (counter<14)
{
	if (((quality-reps[counter])>=0)&&(diff>(quality-reps[counter])))
	{	selIndx = counter;
		diff = quality-reps[counter];
	}
	counter++;
}

dashReceiver->representation        = dashReceiver->adaptationSet->GetRepresentation().at(selIndx);

    dashReceiver->representationStream  = dashReceiver->adaptationSetStream->GetRepresentationStream(dashReceiver->representation);
dashReceiver->DownloadInitSegment(dashReceiver->GetRepresentation());
//the above 3 statements are necessary for changing quality manually

outputfile<<"\nQuality: "<<quality<<"\nreps[selIndx]: "<< reps[selIndx]<<"\ntime: "<<media->GetRevisedTime(dashReceiver->buffer->getTimeWasted())<<endl;

std::cout<<"\nQuality: "<<quality<<"\nreps[selIndx]: "<< reps[selIndx]<<"\n";
dashReceiver->NotifySegmentDownloaded();       
 media = dashReceiver->GetNextSegment();
    
}
outputfile.close();

    dashReceiver->buffer->SetEOS(true);
    return NULL;
}
